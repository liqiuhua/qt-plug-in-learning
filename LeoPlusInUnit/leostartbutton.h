#ifndef LEOSTARTBUTTON_H
#define LEOSTARTBUTTON_H

#include <QWidget>

class LeoStartButton : public QWidget
{
    Q_OBJECT

public:
    explicit LeoStartButton(QWidget *parent = 0);
protected:
    void paintEvent(QPaintEvent *ev) Q_DECL_OVERRIDE; // 重写 paintEvent ，这是将图标重新绘制
    void mousePressEvent(QMouseEvent *ev) Q_DECL_OVERRIDE; // 重写鼠标事件处理方法，根据鼠标按键来触发对应的操作
    void mouseReleaseEvent(QMouseEvent *ev) Q_DECL_OVERRIDE; //重写鼠标释放事件处理方法，暂时未使用，需要可以自己添加
signals:
    void toggled();
private:
    bool m_bPressed = false; //鼠标左键按钮是否点击
    QPixmap m_pixmapStart,m_pixmapStop;//按钮的两种形式
};

#endif // LEOSTARTBUTTON_H
