#ifndef LEOWIFISIGNAL_H
#define LEOWIFISIGNAL_H

#include <QWidget>

class LeoWifiSignal : public QWidget
{
    Q_OBJECT

public:
    LeoWifiSignal(QWidget *parent = 0);
};

#endif // LEOWIFISIGNAL_H
