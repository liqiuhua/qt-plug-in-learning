#include "leostartbutton.h"
#include <QMouseEvent>
#include <QPainter>
#include <qdebug.h>
#define ICON_START ":/img/start-96.png"
#define ICON_STOP ":/img/stop-96.png"

LeoStartButton::LeoStartButton(QWidget *parent) :
    QWidget(parent)
{
    m_pixmapStart.load(ICON_START);
    m_pixmapStop.load(ICON_STOP);
}

void LeoStartButton::paintEvent(QPaintEvent *ev)
{
    QPainter painter(this);
    if(m_bPressed)
    {
        resize(m_pixmapStop.size());
        painter.drawPixmap(0,0,m_pixmapStop);
    }
    else
    {
        resize(m_pixmapStart.size());
        painter.drawPixmap(0,0,m_pixmapStart);
    }
}

void LeoStartButton::mousePressEvent(QMouseEvent *ev)
{
    if( ev->button() == Qt::LeftButton)
    {
        if(m_bPressed)
        {
            m_bPressed = false;
        }
        else
        {
            m_bPressed = true;
        }
        this->update(); //此处必须update，不然不会更新显示
    }

}

void LeoStartButton::mouseReleaseEvent(QMouseEvent *ev)
{

}
