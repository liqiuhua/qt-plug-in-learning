#include "leostartbuttonplugin.h"
#include "leowifisignalplugin.h"
#include "leoplusinunit.h"

LeoPlusInUnit::LeoPlusInUnit(QObject *parent)
    : QObject(parent)
{
    m_widgets.append(new LeoStartButtonPlugin(this));
    m_widgets.append(new LeoWifiSignalPlugin(this));

}

QList<QDesignerCustomWidgetInterface*> LeoPlusInUnit::customWidgets() const
{
    return m_widgets;
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(leoplusinunitplugin, LeoPlusInUnit)
#endif // QT_VERSION < 0x050000
