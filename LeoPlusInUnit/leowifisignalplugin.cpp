#include "leowifisignal.h"
#include "leowifisignalplugin.h"

#include <QtPlugin>

LeoWifiSignalPlugin::LeoWifiSignalPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void LeoWifiSignalPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool LeoWifiSignalPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *LeoWifiSignalPlugin::createWidget(QWidget *parent)
{
    return new LeoWifiSignal(parent);
}

QString LeoWifiSignalPlugin::name() const
{
    return QLatin1String("LeoWifiSignal");
}

QString LeoWifiSignalPlugin::group() const
{
    return QLatin1String("");
}

QIcon LeoWifiSignalPlugin::icon() const
{
    return QIcon();
}

QString LeoWifiSignalPlugin::toolTip() const
{
    return QLatin1String("");
}

QString LeoWifiSignalPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool LeoWifiSignalPlugin::isContainer() const
{
    return false;
}

QString LeoWifiSignalPlugin::domXml() const
{
    return QLatin1String("<widget class=\"LeoWifiSignal\" name=\"leoWifiSignal\">\n</widget>\n");
}

QString LeoWifiSignalPlugin::includeFile() const
{
    return QLatin1String("leowifisignal.h");
}

