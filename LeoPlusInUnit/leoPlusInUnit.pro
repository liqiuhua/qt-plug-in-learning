CONFIG      += plugin debug_and_release
TARGET      = $$qtLibraryTarget(leoplusinunitplugin)
TEMPLATE    = lib

HEADERS     = leostartbuttonplugin.h leowifisignalplugin.h leoplusinunit.h
SOURCES     = leostartbuttonplugin.cpp leowifisignalplugin.cpp leoplusinunit.cpp
RESOURCES   = icons.qrc
LIBS        += -L. 

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS    += target

include(leostartbutton.pri)
include(leowifisignal.pri)
