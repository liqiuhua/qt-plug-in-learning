#include "leostartbutton.h"
#include "leostartbuttonplugin.h"

#include <QtPlugin>

LeoStartButtonPlugin::LeoStartButtonPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void LeoStartButtonPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool LeoStartButtonPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *LeoStartButtonPlugin::createWidget(QWidget *parent)
{
    return new LeoStartButton(parent);
}

QString LeoStartButtonPlugin::name() const
{
    return QLatin1String("LeoStartButton");
}

QString LeoStartButtonPlugin::group() const
{
    return QLatin1String("");
}

QIcon LeoStartButtonPlugin::icon() const
{
    return QIcon(QLatin1String(":/img/start-96.png"));
}

QString LeoStartButtonPlugin::toolTip() const
{
    return QLatin1String("");
}

QString LeoStartButtonPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool LeoStartButtonPlugin::isContainer() const
{
    return false;
}

QString LeoStartButtonPlugin::domXml() const
{
    return QLatin1String("<widget class=\"LeoStartButton\" name=\"leoStartButton\">\n</widget>\n");
}

QString LeoStartButtonPlugin::includeFile() const
{
    return QLatin1String("leostartbutton.h");
}

