#ifndef CSTARTBUTTONTEST_H
#define CSTARTBUTTONTEST_H

#include <QWidget>
#include "leostartbutton.h"
class CStartButtonTest : public QWidget
{
    Q_OBJECT

public:
    CStartButtonTest(QWidget *parent = nullptr);
    ~CStartButtonTest();

private:
    LeoStartButton * m_leoStartButton =  nullptr;
};
#endif // CSTARTBUTTONTEST_H
