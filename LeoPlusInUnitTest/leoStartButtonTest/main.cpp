#include "startbuttontest.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CStartButtonTest w;
    w.show();
    return a.exec();
}
