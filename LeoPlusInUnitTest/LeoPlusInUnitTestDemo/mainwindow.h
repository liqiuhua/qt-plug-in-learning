#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QListWidget>
#include <QListWidgetItem>
#include <QDesignerCustomWidgetInterface>
class LeoPlusIUnit;
class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    bool loadPlugin();
    LeoPlusIUnit *m_LeoPlusInUnit;
    QList<QDesignerCustomWidgetInterface*> m_pluginsWidgets;
    QListWidget * m_plugsinListWidget;
    QStringList m_plugsinListNames;
};
#endif // MAINWINDOW_H
